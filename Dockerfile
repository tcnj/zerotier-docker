FROM ubuntu:bionic

RUN apt update && apt install curl gpg sudo -y && curl -s 'https://download.zerotier.com/contact@zerotier.com.gpg' | gpg --import && if z=$(curl -s 'https://install.zerotier.com/' | gpg); then echo "$z" | sudo bash; fi && rm /var/lib/zerotier-one/* -r

ENTRYPOINT ["/usr/sbin/zerotier-one", "-U"]